import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuditMiddleware } from 'src/middlewares/audit.middleware';
import { JwtModule } from '@nestjs/jwt';
import { User } from 'src/entity/person.entity';
import { UserLogin } from 'src/entity/login.entity';
import { Book } from 'src/entity/Book.entity';
import { BookEntries } from 'src/entity/entries.entity';
import { BookController } from './Booklist.controller';
import { BookService } from './Booklist.service';


@Module({
  imports: [TypeOrmModule.forFeature([User, UserLogin,Book,BookEntries]),JwtModule.register({
    secret:'secret',
    signOptions:{expiresIn:'1hr'}
})],
  controllers: [BookController],
  providers: [BookService]
})

export class BookModule implements NestModule{
  configure(consumer: MiddlewareConsumer) {
     consumer
      .apply(AuditMiddleware)
      .forRoutes({path: 'book/*', method: RequestMethod.DELETE})
  }
}



















/*export class BookModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}*/
