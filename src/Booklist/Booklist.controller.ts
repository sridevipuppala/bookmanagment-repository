import { Body, Controller, Delete, ForbiddenException, Get, HttpException, HttpStatus, Param, Post, Put, Req, Res, UsePipes, ValidationPipe } from "@nestjs/common";
import { NOTFOUND } from "src/custom/NOTFOUND.exception";
import {  Request, Response } from 'express';
import { BookService } from "./Booklist.service";
import { User } from "src/entity/person.entity";
import { Book } from "src/entity/Book.entity";
import { BookEntries } from "src/entity/entries.entity";
import { UserLogin } from "src/entity/login.entity";


@Controller('book')
@UsePipes(ValidationPipe)
export class BookController {
    constructor(private readonly bookService:BookService) {}

    @Post('/user') 
    addUser(@Body() data: User):Promise<User> {
        return this.bookService.addUser(data);
    }

    @Post('/book') 
    addBook(@Body() data: Book):Promise<Book> {
        return this.bookService.addBook(data);
    } 
    @Post('/bookEntries') 
    addBookEntires(@Body() data: BookEntries):Promise<BookEntries> {
        return this.bookService.addBookEntries(data);
    } 

    //user Get methods
    @Get('/user')
    getAllUsers(): Promise<User[]> {
        return this.bookService.getAllUsers();
    }

   // @Get('/user/id/:id')
    //getUserById(@Param('id') id: number, ParseIntPipe): Promise<User> {
        //return this.bookService.getUserById(id)
    //}

    @Get('/user/name/:name')
    getUserByName(@Param('name') name: string): Promise<User> {
        return this.bookService.getUserByName(name);
    }


    @Post('/login')
    async login(@Body() login: UserLogin,@Res({passthrough:true})response:Response) {
      return this.bookService.findOne(login,response);
    }

    @Get('/userlogin')
    async users(@Req() request:Request){
      return this.bookService.findUser(request);
    }

    @Post('/logout')
    async logout(@Res({passthrough:true}) response:Response){
       return this.bookService.logout(response);
    }

    // book get methods
    @Get('/book')
    getAllBooks(): Promise<Book[]> {
        return this.bookService.getAllBooks();
    }

    @Get('/book/:id')
    getBookById(@Param('id') id: number, ParseIntPipe): Promise<Book> {
        return this.bookService.getBookById(id)
         .then((result) => {
            if (result) {     
              return result;      
            } else {     
              throw new HttpException('book not found', HttpStatus.NOT_FOUND);     
            }     
          })     
          .catch (() => {     
            throw new HttpException('book not found', HttpStatus.NOT_FOUND);     
          });
    }

    @Get('/book/name/:name')
    getBookByName(@Param('name') name: string): Promise<Book> {
        return this.bookService.getBookByName(name)

        .then((result) => {
            if (result) {     
              return result;      
            } else {     
               
              throw new NOTFOUND();  
            }     
          })     
          .catch (() => {     
            
            throw new NOTFOUND();    
          });
    }

    // book entries get methods
    @Get('/bookEntries')
    getAllBookEntries(): Promise<BookEntries[]> {
        return this.bookService.getAllBookEntries();
    }

    @Get('/bookEntries/name/:name')
    getBookEntriesByUsername(@Param('name') name: string): Promise<BookEntries> {
        return this.bookService.getBookEntrieByUserName(name)

        .then((result) => {
            if (result) {     
              return result;      
            } else {     
               
              throw new ForbiddenException();  
            }     
          })     
          .catch (() => {     
            
            throw new ForbiddenException();    
          });
    }

    @Delete('/user/:id')
    deleteUserById(@Param('id') id: number){
        return this.bookService.deleteUserById(id);
    }

    @Delete('/book/:id')
    deleteBookById(@Param('id') id: number){
        return this.bookService.deleteBookById(id);
    }

    @Delete('/bookEntries/:id')
    deleteBookEntryById(@Param('id') id: number){
        return this.bookService.deleteBookEntryById(id);
    }

    @Put('/user/:id')
    updateUser(@Param('id') id: number,@Body() user:User) {
        return this.bookService.updateUser(id,user);
    }

    @Put('/book/:id')
    updateBook(@Param('id') id: number,@Body() book:Book) {
        return this.bookService.updateBook(id,book);
    }

    @Put('/bookEntries/:id')
    updateBookEntry(@Param('id') id: number,@Body() bookEntry:BookEntries) {
        return this.bookService.updateBookEntry(id,bookEntry);
    } 
}