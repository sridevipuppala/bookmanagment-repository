import { BadRequestException, Injectable, Logger, Res, UnauthorizedException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectRepository } from "@nestjs/typeorm";
import {  Repository } from "typeorm";
import * as bcrypt from 'bcryptjs';
import { Response,Request } from 'express';
import { UserLogin } from "src/entity/login.entity";
import { User } from "src/entity/person.entity";
import { Book } from "src/entity/Book.entity";
import { BookEntries } from "src/entity/entries.entity";

@Injectable()
export class BookService {

  logger:Logger;

    constructor(
        @InjectRepository(UserLogin)
        private loginRepository: Repository<UserLogin>,
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(Book)
        private bookRepository: Repository<Book>,
        @InjectRepository(BookEntries)
        private entryRepository: Repository<BookEntries>,
        private jwtService:JwtService
        
    ) {
      this.logger=new Logger();
    }

    async addUser(data: User): Promise<User> {
        const pass = await bcrypt.hash(data.password, 10);
       const user = new User();
       const login = new UserLogin();
       user.name= login.name = data.name;
       user.password = login.password = pass;
       user.phoneNo = data.phoneNo
       user.email = data.email;
       user.addressline1 = data.addressline1;
       user.addressline2 = data.addressline2;
       user.state = data.state;
       user.country = data.country;
       user.pinCode = data.pinCode;
       user.login = login;

       return this.userRepository.save(user);
    } 


    async findOne(data: UserLogin,response:Response) {
        const user = await this.userRepository.findOne({ name: data.name });
        console.log(user);
        if (!user) {
          throw new BadRequestException('invalid credentials');
        }
        if (!(await bcrypt.compare(data.password, user.password))) {
          throw new BadRequestException('invalid ');
        }
        const jwt = await this.jwtService.signAsync({id:user.id});
        response.cookie('jwt',jwt,{httpOnly:true});
       return{
           message:'success'
       };
    
      }
    
      async findUser(request: Request){
        try {
          const cookie= request.cookies['jwt'];
          const data = await this.jwtService.verifyAsync(cookie);
          if(!data) {
            throw new UnauthorizedException();
          }
          const user = await this.userRepository.findOne({id: data.id});
          const {password, ...result} = user;
          
          return result;
          } catch(e){
            throw new UnauthorizedException();
          }
      }
    
      async logout(@Res({passthrough:true}) response:Response){
        response.clearCookie('jwt');
        return {
          message: 'logout'
        }
      }

    addBook(bookdata: Book) {
        const book = new Book();
        book.bookName = bookdata.bookName;
        book.bookAuthor = bookdata.bookAuthor;
        book.ISBNNumber = bookdata.ISBNNumber;
        book.noOfPages = bookdata.noOfPages;

        return this.bookRepository.save(book);
    } 

    async addBookEntries(info: BookEntries) {
        const userData: User = await this.userRepository.findOne({
            name: info.userName
        });
        const data = new BookEntries();
        data.userName = info.userName;
        data.bookISBN = info.bookISBN;
        data.dateOfFinishing = info.dateOfFinishing;
        data.review = info.review;
        data.URL = info.URL;
        data.addUser(userData);

        const bookData = await this.bookRepository.findOne({
            ISBNNumber: info.bookISBN
        });
        info.bookName = bookData.bookName;
        data.bookName = bookData.bookName;

        return await this.entryRepository.save(info);

    }

    //get methods for user
    getAllUsers(): Promise<User[]> {
      this.logger.log('getAllUsers is triggered!');
        return this.userRepository.find();
    }

    //getUserById(id: number) {
        //this.logger.log('get userbyid is triggered!');
       // return this.userRepository.findOne({id:id});
    //}

    getUserByName(name: string) {
        this.logger.log('get userbyname is triggered!');
        return this.userRepository.findOne({name:name});
    }

    // get methods for book
    getAllBooks() {
        this.logger.log('getAllBooks is triggered!');
        return this.bookRepository.find();
    }

    getBookById(id: number) {
        this.logger.log('get bookbyid is triggered!');
        return this.bookRepository.findOne({bookId:id})
    }

    getBookByName(name: string) {
        this.logger.log('get book by name is triggered!');
        return this.bookRepository.findOne({bookName:name});
    }

    //get methods for book entries
    getAllBookEntries() {
        this.logger.log('getAllbookentries is triggered!');
        return this.entryRepository.find();
    }

    getBookEntrieByUserName(name: string){
        this.logger.log('get bookentries by username is triggered!');
        return this.entryRepository.findOne({userName:name})
    } 

    deleteUserById(id: number) {
        this.logger.log('get userby id is triggered!');
        return this.userRepository.delete({id})
    }

    deleteBookById(id: number) {
        this.logger.log('DeleteUserById is triggered!');
        return this.bookRepository.delete({bookId:id})
    }

    async deleteBookEntryById(id: number) {
      return await this.entryRepository.delete({id})
    }

    updateUser(id:number, data:User) {
        const user = new User();
        const login = new UserLogin();
        user.name=login.name= data.name;
        user.password =login.password= data.password;
        user.phoneNo = data.phoneNo
        user.email = data.email;
        user.addressline1 = data.addressline1;
        user.addressline2 = data.addressline2;
        user.state = data.state;
        user.country = data.country;
        user.pinCode = data.pinCode;
        user.login = login;
        this.loginRepository.update({id:id},login);
        this.userRepository.update({id:id}, data);
        return 'user details updated sucessfully';
    }

    updateBook(id:number,bookdata: Book) {
        const book = new Book();
        book.bookName = bookdata.bookName;
        book.bookAuthor = bookdata.bookAuthor;
        book.ISBNNumber = bookdata.ISBNNumber;
        book.noOfPages = bookdata.noOfPages;

        this.bookRepository.update({bookId:id}, book);
        return 'book details updated sucessfully';

    }

   async updateBookEntry(id: number,info: BookEntries) {
        const userData: User = await this.userRepository.findOne({
            name: info.userName
        });
        const data = new BookEntries();
        data.userName = info.userName;
        data.bookISBN = info.bookISBN;
        data.dateOfFinishing = info.dateOfFinishing;
        data.review = info.review;
        data.URL = info.URL;
        data.addUser(userData);

        const bookData = await this.bookRepository.findOne({
            ISBNNumber: info.bookISBN
        });
        info.bookName = bookData.bookName;
        data.bookName = bookData.bookName;

        this.entryRepository.update({id: id}, info)
        return 'Entry updated sucessfully';
    }
}
