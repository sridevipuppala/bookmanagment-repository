import { CACHE_MANAGER, Controller, Get, Inject } from '@nestjs/common';
import { AppService } from './app.service';
import { Profile } from './shared/models/Profile';
import { Cache } from 'cache-manager';


@Controller('app')
export class AppController {
  fakeValue = "my book is book";

  fakeModel: Profile = {
    name: "Theend",
    email: 's@gmail.com'
  }
  constructor(@Inject(CACHE_MANAGER) private cacheManager:Cache, private readonly appService: AppService) { }

  @Get()
  async getSimpleString() {
    var value = await this.cacheManager.get('sVal');
    if (value) {
      return {
        data: value,
        LoadsFrom: 'redis Cache'
      }
    }
    await this.cacheManager.set('sVal', this.fakeValue, { ttl: 300 });
    return {
      data: this.fakeValue,
      LoadFrom: 'fakedb'
    }
  }

  @Get('getUser')
  async getUserObj() {
    var profile = await this.cacheManager.get<Profile>('ObjUser');
    if (profile) {
      return {
        data: profile,
        LoadFrom: 'redis Cache'
      }
    }
    await this.cacheManager.set<Profile>('ObjUser', this.fakeModel, { ttl: 300 })
    return {
      data: this.fakeModel,
      LoadFrom: 'fake DB'
    }
  }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
