import { ApiProperty } from "@nestjs/swagger";
import { IsInt, IsString } from "class-validator";
import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export class Book {
    
    @PrimaryGeneratedColumn()
    bookId: number;

    @ApiProperty({
        type: String,
        description: 'The bookName of the book',
        default: '',
    })
    
    @Column()
    @IsString()
    bookName: string;

    @ApiProperty({
        type: String,
        description: 'The author of the book',
        default: '',
    })
    
    @Column()
    @IsString()
    bookAuthor: string;

    @ApiProperty({
        type: Number,
        description: 'The ISBN Number of the book',
        default: '',
    })
    
    @Column()
    @IsInt()
    ISBNNumber: number;

    @ApiProperty({
        type: Number,
        description: 'The noOfPages of the book',
        default: '',
    })
    
    @Column()
    @IsInt()
    noOfPages: number;

}












