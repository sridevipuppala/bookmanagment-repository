import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./person.entity";

@Entity()
export class UserLogin {

    @PrimaryGeneratedColumn()
   
    id: number;

    @ApiProperty({
      type: String,
      description: 'The name of the user',
      default: '',
    })
    @Column()
    @IsString()
    name: string;

    @ApiProperty({
      type: String,
      description: 'The password of the user',
      default: '',
    })
    @Column()
    @IsString()
    password: string;

    @OneToOne( ()=> User, user =>user.login, {
        onDelete: 'CASCADE', onUpdate: 'CASCADE'
    })
    @JoinColumn()
    user:User;

}























/*import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Register {

  @PrimaryGeneratedColumn()
  id:number;

  
  @Column()
  name: string;

  
  @Column()
  password: string;
 
  @OneToOne(() => Person, (person) => person.register)
  person: Person;
  
}*/