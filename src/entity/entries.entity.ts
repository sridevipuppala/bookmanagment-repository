import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './person.entity';

@Entity()
export class BookEntries {

  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({
    type: String,
    description: 'The userName of the user',
    default: '',
  })
  
  @Column()
  userName: string;

  @ApiProperty({
    type: Number,
    description: 'The bookISBN of the user',
    default: '',
  })
  
  @Column()
  bookISBN: number;
  @Column()
  bookName: String;

  @ApiProperty({
    type: String,
    description: 'The dateOfFinishing of the user',
    default: '',
  })
  
  @Column()
  dateOfFinishing: string;

  @ApiProperty({
    type: String,
    description: 'The review of the user',
    default: '',
  })
  
  @Column()
  review: string;

  @ApiProperty({
    type: String,
    description: 'The URL of the user',
    default: '',
  })
  
  @Column()
  URL: string;

  @ManyToMany(() => User, user => user.entry,{
    cascade: true, onUpdate:'CASCADE'
  })
  @JoinTable()
  user:User[];

 async addUser(user:User): Promise<User> {
  if (this.user == null) {
     this.user = new Array<User>();
    }
     this.user.push(user);
     return user;
  } 


}