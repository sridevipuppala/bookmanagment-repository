import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsInt, IsString } from 'class-validator';
import { Column, Entity, JoinColumn, ManyToMany, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { BookEntries } from './entries.entity';

import { UserLogin } from './login.entity';

@Entity()
export class User {

  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({
    type: String,
    description: 'The name of the user',
    default: '',
  })
  
  @Column()
  @IsString()
  name: string;

  @ApiProperty({
    type: String,
    description: 'The password of the user',
    default: '',
  })
  
  @Column()
  @IsString()
  password: string;

  @ApiProperty({
    type: Number,
    description: 'The phoneNo of the user',
    default: '',
  })
  
  @Column()
  @IsInt()
  phoneNo: number;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  
  @Column()
  @IsString()
  email: String;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  
  @Column()
  @IsString()
  addressline1: String;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  
  @Column()
  @IsString()
  addressline2: String;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  
  @Column()
  @IsString()
  state: String;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  
  @Column()
  @IsString()
  country: String;

  @ApiProperty({
    type: Number,
    description: 'The address of the user',
    default: '',
  })
  
  @Column()
  @IsInt()
  pinCode: Number;

  @OneToOne( () => UserLogin, login => login.user, {
      cascade: true,
  })
  login: UserLogin;

  @ManyToMany(() => BookEntries, (entry) => entry.user)
  entry:BookEntries;


}

