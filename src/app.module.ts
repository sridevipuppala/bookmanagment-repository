import { CacheModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BookModule } from './Booklist/Booklist.module';
import { Book } from './entity/Book.entity';
import * as redistore from  'cache-manager-redis-store';
import { BookEntries } from './entity/entries.entity';
import { UserLogin } from './entity/login.entity';
import { User } from './entity/person.entity';

@Module({
  imports: [CacheModule.register(),BookModule, TypeOrmModule.forRoot({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'root',
    database: 'book',
    autoLoadEntities: true,
    entities: [Book, BookEntries,UserLogin,User],
    synchronize: true
  }),],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
