import { Injectable, Logger } from '@nestjs/common';

@Injectable()
export class AppService {
  Logger:Logger;
  constructor(){
    this.Logger=new Logger();
  }
  getHello(): string {
    this.Logger.log('get is triggred')
    return 'Hello World!..I'm here';
  }
}
